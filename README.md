# Coding challenge

REST-service server implementation for IBM Client Innovation Center Coding Challenge

**Short description:** REST-service server (working on localhost on port 8080) which reads data from CSV file 'Film_Locations_in_San_Francisco.csv', processes it and shows data in JSON format
{"title", "locations"}. If requested filtering data by title is also possible.

## Getting started

### How to build

Program is built by executing following command in "challenge" directory: mvn clean package

### How to run

Program is run by executing following command in "challenge" directory: java -jar target/challenge-1.0-SNAPSHOT.jar server

### How to use

Now when the program is built and runs, it is ready to use. Go to a web browser and enter localhost:8080/. The results will be showed.
In order to query (filter) data by title, **localhost:8080/?title=wanted_title** URL needs to be entered. Wanted title represents the title user wants to search for.

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Author

* **Ammar Voloder**