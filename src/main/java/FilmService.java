import com.codahale.metrics.health.HealthCheckRegistry;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FilmService extends Application<Configuration> {

    public static void main(String[] args) throws Exception {
        new FilmService().run(args);
    }

    public void initialize(Bootstrap bootstrap) {}

    public void run(Configuration configuration, Environment environment) throws Exception {

        List<ResultObject> list = new ArrayList<>();

        HealthCheckRegistry registry = new HealthCheckRegistry();

        try{

            /*
             Loading our .csv file in order to process it.
            */
            InputStream inputStream = FilmService.class.getResourceAsStream("/Film_Locations_in_San_Francisco.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            /*
            Reading .csv file while skipping the very first line (header)
            */
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

            String[] nextRecord;


            while((nextRecord = csvReader.readNext()) != null){
                ResultObject rObject = new ResultObject(nextRecord[0], nextRecord[2]);
                list.add(rObject);
                environment.healthChecks().register("ServiceHealthCheck", new CustomHealthCheck(rObject));
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        environment.jersey().register(new FilmResource(list));
    }
}
