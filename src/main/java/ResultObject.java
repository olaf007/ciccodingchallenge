import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultObject {

    private String title;
    private String location;

    public ResultObject(String title, String location){
        this.title = title;
        this.location = location;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
