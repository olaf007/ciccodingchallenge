import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class FilmResource {

    private List<ResultObject> resultList;

    public FilmResource(List<ResultObject> list){
        this.resultList = list;
    }

    @GET
    public List<ResultObject> filter(@DefaultValue("") @QueryParam("title") String title){
        if(title.isEmpty()){
            return resultList;
        }
        else{
            List<ResultObject> filteredList = new ArrayList<ResultObject>();
            for (ResultObject r : resultList) {
                if (r.getTitle().contains(title)) {
                    filteredList.add(r);
                }
            }
            return filteredList;
        }
    }
}
