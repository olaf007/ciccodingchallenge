import com.codahale.metrics.health.HealthCheck;

public class CustomHealthCheck extends HealthCheck{


   private ResultObject resultObject;

   public CustomHealthCheck(ResultObject resultObject){
       super();
       this.resultObject = resultObject;
   }

    @Override
    protected Result check() throws Exception {
        ResultObject result = new ResultObject(resultObject.getTitle(), resultObject.getLocation());
        if(!resultObject.getTitle().equals(result.getTitle())){
            return Result.unhealthy("No such title");
        }

        return Result.healthy();
    }
}
